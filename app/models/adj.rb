class Adj < ActiveRecord::Base
  self.table_name  = 'INV_StockAdjustment'
  self.primary_key = 'StockAdjustmentId'
  has_many :lines, foreign_key: :StockAdjustmentLineId, class_name: "AdjLine"
end
