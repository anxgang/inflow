class So < ActiveRecord::Base
  self.table_name  = 'So_SalesOrder'
  self.primary_key = 'SalesOrderId'
  has_many :lines, foreign_key: :SalesOrderLineId, class_name: "SoLine"
end
