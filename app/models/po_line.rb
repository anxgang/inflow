class PoLine < ActiveRecord::Base
  self.table_name  = 'Po_PurchaseOrder_Line'
  self.primary_key = 'PurchaseOrderLineId'
  belongs_to :po, foreign_key: :PurchaseOrderLineId
end
