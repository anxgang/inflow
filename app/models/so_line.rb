class SoLine < ActiveRecord::Base
  self.table_name  = 'So_SalesOrder_Line'
  self.primary_key = 'SalesOrderLineId'
  belongs_to :so, foreign_key: :SalesOrderLineId
end
