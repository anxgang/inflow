class PagesController < ApplicationController
  def index
    @so_lines = SoLine.includes(:so).order("So_SalesOrder.OrderDate desc")
    @po_lines = PoLine.includes(:po).order("Po_PurchaseOrder.OrderDate desc")

    lines = []
    @so_lines.each do |line|
      lines << { 
        OrderDate: line.so.OrderDate, 
        ProdId: line.ProdId, 
        QuantityDisplay: line.QuantityDisplay,
        Type: 'PO'
      }
    end
    @po_lines.each do |line|
      lines << { 
        OrderDate: line.po.OrderDate, 
        ProdId: line.ProdId, 
        QuantityDisplay: line.QuantityDisplay,
        Type: 'PO'
      }
    end

    @lines = lines.sort{|x| -x[:OrderDate].to_i }
  end
end
