class AdjLine < ActiveRecord::Base
  self.table_name  = 'INV_StockAdjustment_Line'
  self.primary_key = 'StockAdjustmentLineId'
  belongs_to :adj, foreign_key: :StockAdjustmentLineId
end

