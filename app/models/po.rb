class Po < ActiveRecord::Base
  self.table_name  = 'Po_PurchaseOrder'
  self.primary_key = 'PurchaseOrderId'
  has_many :lines, foreign_key: :PurchaseOrderLineId, class_name: "PoLine"
end
